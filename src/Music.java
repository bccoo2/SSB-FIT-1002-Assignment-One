
public class Music {
    private String musicType;

    public Music() {
        this.musicType = null;
    }

    public Music(String sType) {
        setMusicType(sType);
    }

    public String getMusicType() {
        return this.musicType;
    }

    public void setMusicType(String sType) {
        if (sType.isEmpty() || sType.equals(" "))
            System.out.print("\nInvalid DJ Name");
        else
            this.musicType = sType;
    }

    @Override
    public String toString() {
        return (musicType);
    }
}
