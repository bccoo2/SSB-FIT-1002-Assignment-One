
public class Transaction {
    private Student aStudent;
    private DJ aDJ;
    private Date aDate;

    public Transaction() {
        this.aStudent = null;
        this.aDJ = null;
        this.aDate = null;
    }

    public Transaction(Student oStudent, DJ oDJ, Date oDate) {
        this.aStudent = oStudent;
        this.aDJ = oDJ;
        this.aDJ.setHired(true);
        this.aDate = oDate;
    }

    public Student getaStudent() {
        return this.aStudent;
    }

    public void setaStudent(Student oStudent) {
        this.aStudent = oStudent;
    }

    public DJ getaDJ() {
        return this.aDJ;
    }

    public void setaDJ(DJ oDJ) {
        this.aDJ = oDJ;
    }

    public Date getaDate() {
        return this.aDate;
    }

    public void setaDate(Date oDate) {
        this.aDate = oDate;
    }

    @Override
    public String toString() {
        return (aStudent.toString() + " has hired\n\t" + aDJ.toStringB() + "\n\tOn: " + aDate.toString());
    }
}
