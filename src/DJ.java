
public class DJ {
    private String djName;
    private boolean isHired;
    private Music[] musicGeneras = new Music[]{
            new Music(), new Music(), new Music()
    };

    public DJ() {
        this.djName = null;
        this.isHired = false;
        musicGeneras[0] = null;
        musicGeneras[1] = null;
        musicGeneras[2] = null;
    }

    public DJ(String sName, boolean bHired) {
        setDjName(sName);
        setHired(bHired);
    }

    public DJ(String sName, boolean bHired, String gOne, String gTwo, String gThree) {
        setDjName(sName);
        setHired(bHired);
        musicGeneras[0].setMusicType(gOne);
        musicGeneras[1].setMusicType(gTwo);
        musicGeneras[2].setMusicType(gThree);
    }

    public String getDjName() {
        return this.djName;
    }

    public void setDjName(String sName) {
        if (sName.isEmpty() || sName.equals(" "))
            System.out.print("\nInvalid DJ Name");
        else
            this.djName = sName;
    }

    public boolean isHired() {
        return this.isHired;
    }

    public void setHired(boolean bHired) {
        this.isHired = bHired;
    }

    public String toStringA() {
        return (this.djName + " \n\tGenera's: " + musicGeneras[0].toString() + " & " + this.musicGeneras[1].toString() + " & " + this.musicGeneras[2].toString() + "\n\tAvailable: " + (this.isHired() ? "No" : "Yes"));
    }

    public String toStringB() {
        return (this.djName + " \n\tGenera's: " + this.musicGeneras[0].toString() + " & " + this.musicGeneras[1].toString() + " & " + this.musicGeneras[2].toString());
    }


}
