import java.util.Objects;

public class Date {
    private int day;
    private int month;
    private int year;

    public Date() {
        this.day = 0;
        this.month = 0;
        this.year = 0;
    }

    public Date(int iDay, int iMonth, int iYear) {
        setYear(iYear);
        setMonth(iMonth);
        setDay(iDay);

    }

    private int getDay() {
        return this.day;
    }

    private void setDay(int iDay) {
        if (Objects.equals(this.getMonth(), "January") || Objects.equals(this.getMonth(), "March") || Objects.equals(this.getMonth(), "May") || Objects.equals(this.getMonth(), "July") || Objects.equals(this.getMonth(), "August") || Objects.equals(this.getMonth(), "October") || Objects.equals(this.getMonth(), "December"))
            if (iDay <= 31 && iDay >= 1)
                this.day = iDay;
            else if (Objects.equals(this.getMonth(), "April") || Objects.equals(this.getMonth(), "June") || Objects.equals(this.getMonth(), "September") || Objects.equals(this.getMonth(), "November"))
                if (iDay <= 30 && iDay >= 1)
                    this.day = iDay;
                else if (Objects.equals(this.getMonth(), "February"))
                    if ((this.getYear() % 4 == 0) || (this.getYear() % 100 == 0) || (this.getYear() % 400 == 0))
                        if (iDay <= 29 && iDay >= 1)
                            this.day = iDay;
                        else
                            System.out.println("Invalid Day");
                    else if (iDay <= 28 && iDay >= 1)
                        this.day = iDay;
                    else
                        System.out.println("Invalid Day");
                else {
                    System.out.println("Invalid input");
                }

    }

    private String getMonth() {
        // return this.month as a String
        switch (this.month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "Invalid Month";
        }
    }

    private void setMonth(int iMonth) {
        if (iMonth <= 12 && iMonth >= 1)
            this.month = iMonth;
        else
            System.out.println("Invalid Month");
    }

    private int getYear() {
        return this.year;
    }

    private void setYear(int iYear) {
        if (iYear >= 2016)
            this.year = iYear;
        else
            System.out.println("Invalid Year");
    }

    @Override
    public String toString() {
        return (this.getDay() + " " + this.getMonth() + " " + this.getYear());
    }

}
