import java.util.Objects;
import java.util.Scanner;

public class DJMusicBusiness {
    private static DJ[] aDJs = new DJ[100];
    private static Student[] aStudents = new Student[100];
    private static Transaction[] aTransactions = new Transaction[100];
    private static Date[] aDates = new Date[100];
    private static int aStudentPos = 0;
    private static int aDJPos = 0;
    private static int aTransactionPos = 0;

    static {
        System.out.print("***\tWelcome to DJ Hiring\t***\n");
    }

    public static void main(String[] args) {
        Scanner oScan = new Scanner(System.in);
        boolean bLoop = true;
        // Loop the menu while bLoop is true
        do {
            System.out.print("Choose one OPTION from the menu below\n");
            System.out.print("[1]\tStudent\n");
            System.out.print("[2]\tDJ\n");
            System.out.print("[3]\tTransaction\n");
            System.out.print("[4]\tExit\n");
            System.out.print("Selection: ");
            // Switch menu
            switch (oScan.nextInt()) {
                case 1:
                    // Load Student Options menu
                    studentOptions();
                    break;
                case 2:
                    // Load DJ Options Menu
                    djOptions();
                    break;
                case 3:
                    // Load Transaction Options Menu
                    transactionOptions();
                    break;
                case 4:
                    // Terminate loop
                    bLoop = false;
                    break;
                default:
                    System.out.print("\nInvalid option.\n\n");
            }
        } while (bLoop);

    }

    private static void studentOptions() {
        Scanner oScan = new Scanner(System.in);
        boolean bLoop = true;
        // Loop the menu while bLoop is true
        do {
            System.out.print("Choose one OPTION from the menu below\n");
            System.out.print("[1]\tAdd a new Student\n");
            System.out.print("[2]\tView all students\n");
            System.out.print("[3]\tSearch for a specific student\n");
            System.out.print("[4]\tDelete a specific student\n");
            System.out.print("[5]\tReturn to main menu\n");
            System.out.print("Selection: ");
            // Switch menu
            switch (oScan.nextInt()) {
                case 1: {
                    System.out.print("\nPlease input student name: ");
                    // Scanner.next automatically waits for a valid input
                    String sName = oScan.next();
                    System.out.print("Please input student phone number: ");
                    // Scanner.next automatically waits for a valid input
                    String sPhone = oScan.next();
                    aStudents[aStudentPos] = new Student(sName, sPhone);
                    // Print new student
                    System.out.print("\nStudent: " + aStudents[aStudentPos].toString() + " has been created\n");
                    aStudentPos++;
                }
                break;
                case 2: {
                    boolean bValid = true;
                    int iParity = 0;
                    System.out.print("\nListing all Students: ");
                    for (Student aStudent : aStudents) {
                        if (aStudent != null) {
                            System.out.print("\n" + aStudent.toString() + "\n\n");
                            bValid = true;
                            // Increase parity to validate a false set condition
                            iParity++;
                        } else {
                            if (iParity == 0)
                                bValid = false;
                        }
                    }
                    if (!bValid) {
                        System.out.print("\nNo valid students could be found\n\n");
                    }
                }
                break;
                case 3: {
                    boolean bValid = true;
                    System.out.print("\nPlease enter the name of the student to search for: ");
                    String sName = oScan.next();
                    for (Student aStudent : aStudents) {
                        if (aStudent != null && Objects.equals(aStudent.getName(), sName)) {
                            System.out.print("\n" + aStudent.toString() + "\n\n");
                            bValid = true;
                            break;
                        } else {
                            bValid = false;
                        }
                    }
                    if (!bValid)
                        System.out.print("\nThere are no results to display\n\n");
                }
                break;
                case 4: {
                    boolean bValid = true;
                    System.out.print("\nPlease enter the name of the student to remove: ");
                    String sName = oScan.next();
                    for (int i = 0; i < aStudents.length; i++) {
                        if (aStudents[i] != null && Objects.equals(aStudents[i].getName(), sName)) {
                            aStudents[i] = null;
                            bValid = true;
                            System.out.print("\nStudent has been removed\n\n");
                            break;
                        } else
                            bValid = false;
                    }
                    if (!bValid)
                        System.out.print("\nNo records to remove\n\n");
                }
                break;
                case 5:
                    // Terminate loop
                    bLoop = false;
                    break;
                default:
                    System.out.print("\nInvalid option.\n\n");
            }
        } while (bLoop);
    }

    private static void djOptions() {
        Scanner oScan = new Scanner(System.in);
        boolean bLoop = true;
        // Loop the menu while bLoop is true
        do {
            System.out.print("Choose one OPTION from the menu below\n");
            System.out.print("[1]\tAdd a new DJ to the System\n");
            System.out.print("[2]\tView all Hired DJs\n");
            System.out.print("[3]\tView all available DJs\n");
            System.out.print("[4]\tSearch for a specific DJ (by name)\n");
            System.out.print("[5]\tReturn to main menu\n");
            System.out.print("Selection: ");
            // Switch menu
            switch (oScan.nextInt()) {
                case 1: {
                    System.out.print("\nPlease input DJ name: ");
                    String sName = oScan.next();
                    System.out.print("\n\nPlease input DJ's THREE genera's: ");
                    String sGeneraOne = oScan.next();
                    String sGeneraTwo = oScan.next();
                    String sGeneraThree = oScan.next();
                    aDJs[aDJPos] = new DJ(sName, false, sGeneraOne, sGeneraTwo, sGeneraThree);
                    System.out.print("\nDJ " + aDJs[aDJPos].getDjName() + " has been created\n");
                    aDJPos++;
                }
                break;
                case 2: {
                    boolean bValid = true;
                    int iParity = 0;
                    for (DJ aDJ : aDJs) {
                        if (aDJ != null && aDJ.isHired()) {
                            System.out.print("\n" + aDJ.toStringA() + "\n\n");
                            iParity++;
                        } else {
                            if (iParity == 0) {
                                bValid = false;
                            }
                        }

                    }
                    if (!bValid)
                        System.out.print("\nThere are no available DJs\n\n");
                }
                break;
                case 3: {
                    boolean bValid = true;
                    int iParity = 0;
                    for (DJ aDJ : aDJs) {
                        if (aDJ != null && !aDJ.isHired()) {
                            System.out.print("\n" + aDJ.toStringA() + "\n\n");
                            iParity++;
                            bValid = true;
                        } else {
                            if (iParity == 0) {
                                bValid = false;
                            }
                        }
                    }
                    if (!bValid)
                        System.out.print("\nThere are no DJs to display\n\n");
                }
                break;
                case 4: {
                    System.out.print("\nPlease enter the name of the DJ to search for: ");
                    String sDJName = oScan.next();
                    for (DJ aDJ : aDJs) {
                        if (aDJ != null && Objects.equals(aDJ.getDjName(), sDJName))
                            System.out.print("\n" + aDJ.toStringA() + "\n\n");
                    }
                }
                break;
                case 5:
                    // Terminate loop
                    bLoop = false;
                    break;
                default:
                    System.out.print("\nInvalid option.\n\n");
            }
        } while (bLoop);
    }

    private static void transactionOptions() {
        Scanner oScan = new Scanner(System.in);
        boolean bLoop = true;
        // Loop the menu while bLoop is true
        do {
            System.out.print("Choose one OPTION from the menu below\n");
            System.out.print("[1]\tHire a DJ\n");
            System.out.print("[2]\tView all transactions\n");
            System.out.print("[3]\tReturn to main menu\n");
            System.out.print("Selection: ");
            // Switch menu
            switch (oScan.nextInt()) {
                case 1:
                    hireDJ();
                    break;
                case 2:
                    viewAllTransactions();
                    break;
                case 3:
                    // Terminate loop
                    bLoop = false;
                    break;
                default:
                    System.out.print("\nInvalid option.\n\n");
            }
        } while (bLoop);
    }

    private static void hireDJ() {
        boolean bStudentValid = true;
        boolean bDJValid = true;
        Student aValidStudent = null;
        DJ aValidDJ = null;
        Scanner oScan = new Scanner(System.in);
        System.out.print("\nPlease enter the name of the student who wants to hire a DJ: ");
        String sName = oScan.nextLine();

        for (Student aStudent : aStudents) {
            // Check if the name of the student is a member of aStudent, and that aStudent is valid
            if ((aStudent == null) || !Objects.equals(sName, aStudent.getName())) {
                bStudentValid = false;
            } else {
                bStudentValid = true;
                aValidStudent = aStudent;
                break;
            }
        }
        if (bStudentValid) {
            System.out.print("Please enter the name of the DJ to be hired: ");
            String sDJName = oScan.nextLine();
            // Check if the DJ is available for hire, and that aDJ is valid
            for (DJ aDJ : aDJs) {
                if ((aDJ == null) || (!Objects.equals(aDJ.getDjName(), sDJName) && !aDJ.isHired())) {
                    bDJValid = false;
                } else {
                    bDJValid = true;
                    aValidDJ = aDJ;
                    break;
                }
            }
        } else {
            System.out.print("\nThat student does not exist in the records.");
        }

        if (bDJValid) {
            System.out.print("\nEnter the numerical date for which the DJ is to be hired in the form DD MM YYYY: ");
            String sFullDate = oScan.nextLine();
            String[] sDateSplit = sFullDate.split(" ");
            aDates[aTransactionPos] = new Date(Integer.parseInt(sDateSplit[0]), Integer.parseInt(sDateSplit[1]), Integer.parseInt(sDateSplit[2]));
            aTransactions[aTransactionPos] = new Transaction(aValidStudent, aValidDJ, aDates[aTransactionPos]);
            System.out.print("\n" + aTransactions[aTransactionPos].toString() + "\n\n");
            aTransactionPos++;
        } else {
            System.out.print("\nInvalid DJ Name or DJ has been already hired\n\n");
        }
    }

    private static void viewAllTransactions() {
        // Check if there are any valid transactions
        boolean bValid = true;
        for (Transaction aTransaction : aTransactions) {
            if (aTransaction != null) {
                System.out.print("\n" + aTransaction.toString() + "\n\n");
                bValid = true;
            } else
                bValid = false;
        }
        if (!bValid)
            System.out.print("\nThere are no transactions to view\n\n");
    }
}
