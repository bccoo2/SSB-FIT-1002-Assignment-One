
public class Student {
    private String name;
    private String phoneNumber;

    public Student() {
        this.name = null;
        this.phoneNumber = null;
    }

    public Student(String sName, String sPhone) {
        this.name = sName;
        this.phoneNumber = sPhone;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String sName) {
        if (sName.isEmpty() || sName.equals(" "))
            System.out.print("\nInvalid DJ Name");
        else
            this.name = sName;
    }

    public String getNumber() {
        return this.phoneNumber;
    }

    public void setNumber(String sPhone) {
        this.phoneNumber = sPhone;
    }

    @Override
    public String toString() {
        return (this.name + " (Phone: " + this.phoneNumber + ")");
    }
}
